# Seshat


Gets named entities.


## Design and Considerations

- async non blocking http server;
- cpu-bound operations (nlp) done in process pool;
- batch nlp pipe for performance;
- document id is generated with sha1 hash (could be used to detect duplicates);
- server saves the results on a database (i.e. postgresql but cockroachdb provides better
scalability).
- **horizontal scaling and discovery should be implemented using kubernetes**.

## Installation

### Local Installation

Requirements:
  - python 3.7
  - postgresql

Installs the **seshat** module in the current python environment:

```
pip3 install git+https://bitbucket.org/monolito/seshat.git#egg=seshat
python3 -m spacy download en_core_web_sm
```
## Kubernetes

```
docker-compose up --no-start
docker tag <image_id> localhost:5000/docker_seshat
docker push localhost:5000/docker_seshat
docker tag <image_id> localhost:5000/docker_postgres
docker push localhost:5000/docker_postgres
```

```
minikube start --vm-driver="virtualbox" --insecure-registry=192.168.99.1:5000
sudo ./deploy.sh
```

```
curl -H 'Host: seshat.gateway' $(sudo minikube ip)/
```


## Running

Create a database (postgres):

```
sudo -u postgres psql < ./docker/db/create.sql
```

Run the server app:

```
seshat -h
```

## API

```POST /api/named_entities``` detects named entities of the text passed in
the HTTP request body.

Response:
```
{
    <sha1 id> : {
        <TYPE> : [
            {text, start, end},
            {text, start, end},
            ...
        ]
    }
}
```

# TODO

- Optimizations and enhancements:
    - Implement API endpoint to recover named_entities by document id;
    - Clean up input content before the nlp pipeline;
    - Increase robustness and proper error handling in coroutines;
    - Large files could be chunked and processed by parts;
    - Duplicate large files could be skipped.
- Docker containers ```./docker``` contains docker files (not tested);
- Kubernetes deployment. ```./kubernetes``` contains a proof of concept of a deployment
using **minikube** (not working yet);
- Use **cockroachdb** as backend.

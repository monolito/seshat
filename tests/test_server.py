#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest
from seshat.server import parse_args


def test_pase_args():
    args = parse_args(['-p', '5000'])
    assert args.port == 5000

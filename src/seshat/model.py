from dataclasses import dataclass
import json


@dataclass
class Entity:
    text: str
    start_char: int
    end_char: int


class EntityEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Entity):
            return obj.__dict__
        return json.JSONEncoder.default(self, obj)

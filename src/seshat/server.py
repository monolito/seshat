#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Run `python setup.py install` which will install the command `seshat`
inside your current environment.

"""

import argparse
import asyncio
import hashlib
import json
import logging
import os
import resource
import sys
import time
from asyncio import QueueEmpty
from collections import defaultdict
from concurrent.futures import ProcessPoolExecutor

import asyncpg
import spacy
from sanic import Sanic
from sanic.response import json as json_response, text

from seshat import __version__
from seshat.model import Entity, EntityEncoder

__author__ = "Alex Prudencio"
__copyright__ = "Alex Prudencio"
__license__ = "mit"

_logger = logging.getLogger(__name__)
_nlp = spacy.load('en_core_web_sm')
_executor = ProcessPoolExecutor()
app = Sanic(__name__)
_BATCH_TIMEOUT_SECONDS = 0.01
_BATCH_SIZE_MAX = 100

nlp_worker_queue = asyncio.Queue()


def parse_args(args):
    """Parse command line parameters

    Args:
      args ([str]): command line parameters as list of strings

    Returns:
      :obj:`argparse.Namespace`: command line parameters namespace
    """
    parser = argparse.ArgumentParser(
        description="seshat service")
    parser.add_argument(
        '--version',
        action='version',
        version='seshat {ver}'.format(ver=__version__))
    parser.add_argument(
        "-p",
        dest="port",
        help="port number",
        type=int,
        metavar="INT",
        default=5000)
    parser.add_argument(
        "--db-host",
        dest="db_host",
        help="database host",
        default="localhost")
    parser.add_argument(
        "--db-name",
        dest="db_name",
        help="database name",
        default="seshat")
    parser.add_argument(
        "--db-user",
        dest="db_user",
        help="database user",
        default="seshat")
    parser.add_argument(
        "--db-password",
        dest="db_password",
        help="database password",
        default="seshat")
    parser.add_argument(
        "--db-port",
        dest="db_port",
        help="database port",
        type=int,
        default=5432)
    parser.add_argument(
        "--server-workers",
        dest="server_workers",
        help="server workers",
        type=int,
        metavar="INT",
        default=1)
    parser.add_argument(
        '-v',
        '--verbose',
        dest="loglevel",
        help="set loglevel to INFO",
        action='store_const',
        const=logging.INFO)
    parser.add_argument(
        '-vv',
        '--very-verbose',
        dest="loglevel",
        help="set loglevel to DEBUG",
        action='store_const',
        const=logging.DEBUG)

    return parser.parse_args(args)


args = parse_args(sys.argv[1:])


def setup_logging(loglevel):
    """Setup basic logging

    Args:
      loglevel (int): minimum loglevel for emitting messages
    """
    logformat = "[%(asctime)s] %(levelname)s:%(name)s:%(message)s"
    logging.basicConfig(level=loglevel, stream=sys.stdout,
                        format=logformat, datefmt="%Y-%m-%d %H:%M:%S")


def aggregate_result(id, doc):
    entities = [(ent.label_, Entity(ent.text, ent.start_char, ent.end_char)) for ent in doc.ents]
    entities_by_label = defaultdict(list)

    for (label, entity) in entities:
        entities_by_label[label].append(entity)

    return {id: entities_by_label}


def run_batch(documents):
    ids = [hashlib.sha1(doc).hexdigest() for doc in documents]
    results = [doc for doc in _nlp.pipe(map(lambda d: d.decode('utf-8'), documents), n_threads=-1)]
    return [aggregate_result(doc_id, doc) for (doc_id, doc) in zip(ids, results)]


async def nlp_worker():
    last_time = None
    batch = []

    while True:
        try:
            if last_time and (len(batch) >= _BATCH_SIZE_MAX or time.time() - last_time > _BATCH_TIMEOUT_SECONDS):
                _logger.info("Submitting batch of size %s", len(batch))
                documents = [doc for (_, doc) in batch]
                results = await asyncio.get_running_loop().run_in_executor(_executor, run_batch, documents)
                await save_results(results)

                for ((request_future, _), result) in zip(batch, results):
                    request_future.set_result(result)

                batch = []
                last_time = None

            while len(batch) < _BATCH_SIZE_MAX:
                item = nlp_worker_queue.get_nowait()
                batch.append(item)
                if not last_time:
                    last_time = time.time()
        except QueueEmpty:
            await asyncio.sleep(_BATCH_TIMEOUT_SECONDS)


async def save_results(results):
    async with app.pool.acquire() as connection:
        async with connection.transaction():
            await connection.executemany(
                'INSERT INTO documents VALUES($1, $2) ON CONFLICT DO NOTHING',
                [(list(result.keys())[0], json.dumps(result, cls=EntityEncoder)) for result in results])


@app.listener('before_server_start')
async def init_app(app, loop):
    setup_logging(args.loglevel)
    _ = asyncio.create_task(nlp_worker())
    app.pool = await asyncpg.create_pool(host=args.db_host, port=args.db_port, database=args.db_name, user=args.db_user,
                                         password=args.db_password)

    async with app.pool.acquire() as conn:
        async with conn.transaction():
            try:
                await conn.execute('CREATE TABLE documents (id CHAR(40), entities JSONB)')
            except Exception as e:
                _logger.warning('%s. Skipping.', e)
                pass


def mem_usage():
    return resource.getrusage(resource.RUSAGE_SELF).ru_maxrss / 1024


@app.middleware('request')
async def validate_server_load(request):
    # TODO: should also validate children sub-processes memory.
    memory_total_gb = mem_usage() / 1024

    if memory_total_gb > 3:
        return text('Server is busy', status=503)


@app.route("/api/named_entities", methods=['POST'])
async def named_entities(request):
    msg = request.body
    request_future = asyncio.get_running_loop().create_future()

    # TODO: large files should not be batched.
    nlp_worker_queue.put_nowait((request_future, msg))
    result = await request_future

    return json_response(result, dumps=lambda b: json.dumps(b, cls=EntityEncoder))


def run():
    """Entry point for console_scripts
    """
    app.run(host="0.0.0.0", port=args.port, workers=args.server_workers)


if __name__ == "__main__":
    run()

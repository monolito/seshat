#!/bin/bash

echo "Creating the volume..."

kubectl apply -f ./persistent-volume.yml
kubectl apply -f ./persistent-volume-claim.yml

echo "Creating the database credentials..."

kubectl apply -f ./secret.yml

echo "Creating the postgres deployment and service..."

kubectl create -f ./postgres-deployment.yml
kubectl create -f ./postgres-service.yml

echo "Creating the seshat deployment and service..."

kubectl create -f ./seshat-deployment.yml
kubectl create -f ./seshat-service.yml

echo "Adding the ingress..."

minikube addons enable ingress
kubectl apply -f ./minikube-ingress.yml
